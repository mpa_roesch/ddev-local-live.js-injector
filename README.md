# DDEV-Local Live.js injector

This extension injects [Live.js](https://livejs.com/) or [LiveReload](http://livereload.com/) into any [DDEV-Local](https://ddev.com/ddev-local) domains, enabling live reloading without the need for server-side modifications.

![](preview.gif)

## Installation

### Firefox

1. Download the [latest XPI file](https://dlli-update.vercel.app/latest.xpi)
2. Choose "Continue to Installation"
3. Choose "Add"

### Chromium (Chrome, Edge and Opera)

1. Download the [latest XPI file](https://dlli-update.vercel.app/latest.xpi)
2. Change its extension to ZIP and extract it
3. Go to [chrome://extensions](chrome://extensions), [edge://extensions](edge://extensions) or [opera://extensions](opera://extensions)
4. Enable developer mode
5. Choose "Load unpacked"
6. Select the extracted XPI file

## Usage

Just open any `*.ddev.site` domain, run `grunt dowatch` and you're good to go! The extension will display a small icon in the address bar whenever live reloading is enabled.

You can click on that icon or go to the add-on preferences page to select which resources to monitor for changes (HTML, CSS and/or JavaScript – HTML is disabled by default, as it is still experimental), as well as tweak some other settings.

## Development

### Extension

```Bash
git clone https://bitbucket.org/mpa_roesch/ddev-local-live.js-injector
cd ddev-local-live.js-injector
npm install
npm run start:firefox # or npm run start:chrome
```

With Firefox, the extension will automatically be reloaded whenever there are any modified files. With Chrome, you'll need to reload it manually.

### Server

Since the extension is not being published via [AMO](https://addons.mozilla.org/), a custom server, deployed via [Vercel](https://vercel.com/), is used to keep it up-to-date.

Two environment variables are required: `BITBUCKET_USERNAME` and `BITBUCKET_PASSWORD`. The respective user must have access to this repository.

```Bash
git clone https://bitbucket.org/mpa_roesch/ddev-local-live.js-injector
cd ddev-local-live.js-injector
npm install
ln -s ../node_modules/{@exampledev/new.css,@fontsource/inter} public
npm run start:vercel
```
