export default function handler (req, res) {
  const axios = require('axios')

  axios({
    url: 'https://api.bitbucket.org/2.0/repositories/mpa_roesch/ddev-local-live.js-injector/downloads',
    auth: {
      username: process.env.BITBUCKET_USERNAME,
      password: process.env.BITBUCKET_PASSWORD
    }
  }).then(response => {
    const json = {
      addons: {
        'ddev-local-live.js-injector@eric.jetzt': {
          updates: []
        }
      }
    }

    response.data.values.forEach(x => {
      json.addons['ddev-local-live.js-injector@eric.jetzt'].updates.push({
        version: x.name.match(/[\d.]+/).toString(),
        update_link: 'https://dlli-update.vercel.app/' + x.name
      })
    })

    res.statusCode = 200
    res.setHeader('Content-Type', 'application/json')
    res.json(json)
  })
}
