export default function handler (req, res) {
  const axios = require('axios')

  axios({
    url: 'https://api.bitbucket.org/2.0/repositories/mpa_roesch/ddev-local-live.js-injector/downloads/' + req.query.filename,
    auth: {
      username: process.env.BITBUCKET_USERNAME,
      password: process.env.BITBUCKET_PASSWORD
    },
    responseType: 'arraybuffer'
  }).then(response => {
    res.statusCode = 200
    res.setHeader('Content-Type', 'application/x-xpinstall')
    res.end(response.data)
  })
}
