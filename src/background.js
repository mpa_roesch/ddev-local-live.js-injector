/* eslint-env browser, webextensions */

if (browser.browserSettings) {
  browser.browserSettings.cacheEnabled.set({
    value: false
  }).then(() => {
    browser.runtime.setUninstallURL('https://dlli-update.vercel.app/uninstall.html')
  })
}

browser.storage.sync.get().then(res => {
  if (res.implementation === undefined) browser.storage.sync.set({ implementation: 'livejs' })
  if (res['monitor-html'] === undefined) browser.storage.sync.set({ 'monitor-html': false })
  if (res['monitor-css'] === undefined) browser.storage.sync.set({ 'monitor-css': true })
  if (res['monitor-js'] === undefined) browser.storage.sync.set({ 'monitor-js': true })
  if (res.notification === undefined) browser.storage.sync.set({ notification: false })
  if (res.host === undefined) browser.storage.sync.set({ host: 'localhost' })
  if (res.port === undefined) browser.storage.sync.set({ port: 35729 })
})

browser.runtime.onMessage.addListener((data, sender) => {
  if (data.function === 'showAction') {
    browser.storage.sync.get().then(res => {
      browser.pageAction.setTitle({
        tabId: sender.tab.id,
        title: browser.i18n.getMessage('actionTitle').slice(0, -1) + ' (' + res.implementation.replace('livejs', 'Live.js').replace('livereload', 'LiveReload') + ').'
      })
      browser.pageAction.show(sender.tab.id)
    })
  }
})
