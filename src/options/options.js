/* eslint-env browser, webextensions */

document.addEventListener('DOMContentLoaded', () => {
  document.querySelectorAll("[id^='__MSG_']").forEach(x => {
    x.innerText = browser.i18n.getMessage(x.id.slice(6, -2))
  })

  browser.storage.sync.get().then(res => {
    if (res.implementation) document.getElementById('implementation-' + res.implementation).checked = true
    if (res['monitor-html']) document.getElementById('monitor-html').checked = true
    if (res['monitor-css']) document.getElementById('monitor-css').checked = true
    if (res['monitor-js']) document.getElementById('monitor-js').checked = true
    if (res.notification) document.getElementById('notification').checked = true
    if (res.host) document.getElementById('host').value = res.host
    if (res.port) document.getElementById('port').value = res.port
  })
})

document.querySelectorAll("input[type='radio'], input[type='text'], input[type='number']").forEach(el => {
  el.addEventListener('input', e => {
    const storage = {}
    storage[e.currentTarget.name] = e.currentTarget.value

    browser.storage.sync.set(storage)
  })
})

document.querySelectorAll("input[type='checkbox']").forEach(el => {
  el.addEventListener('input', e => {
    const storage = {}
    storage[e.currentTarget.name] = e.currentTarget.checked

    browser.storage.sync.set(storage)
  })
})

document.getElementById('__MSG_settingsResetDescription__').addEventListener('click', () => {
  browser.storage.sync.set({
    implementation: 'livejs',
    'monitor-html': false,
    'monitor-css': true,
    'monitor-js': true,
    notification: false,
    host: 'localhost',
    port: 35729
  }).then(() => {
    document.dispatchEvent(new Event('DOMContentLoaded'))
  })
})
