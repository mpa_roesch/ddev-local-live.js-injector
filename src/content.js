/* eslint-env browser, webextensions */

if (!window.location.pathname.startsWith('/typo3/')) {
  browser.storage.sync.get().then(res => {
    const script = document.createElement('script')
    const storage = []

    switch (res.implementation) {
      case 'livejs':
        if (res['monitor-html']) storage.push('html')
        if (res['monitor-css']) storage.push('css')
        if (res['monitor-js']) storage.push('js')

        if (!storage.length) return

        if (res.notification) storage.push('notify')

        script.setAttribute('src', browser.runtime.getURL('utilities/implementations/live.js') + '#' + storage.join(','))
        break
      case 'livereload':
        script.setAttribute('src', browser.runtime.getURL('utilities/implementations/livereload.js?host=' + res.host + '&port=' + res.port))
        break
    }

    document.head.appendChild(script)

    browser.runtime.sendMessage({
      function: 'showAction'
    })
  })

  browser.storage.onChanged.addListener(() => {
    location.reload()
  })
}
