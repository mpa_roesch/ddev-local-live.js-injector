# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Documentation for uninstallation page dependencies

### Changed

- Use @fontsource/inter instead of inter-ui

## [1.3.0] - 2021-10-19

### Added

- Changelog file
- Uninstallation page

### Changed

- Use colored PNGs instead of SVG icon
- Update dependencies
- Update package-lock.json to version 2

### Fixed

- Injection of scripts in Chromium
- Overflowing options page

## [1.2.0] - 2021-09-08

### Added

- LiveReload support
- Auto-update server with Vercel deployment

### Changed

- Move implementations into subdirectory
- Simplify internationalization process
- Update dependencies

### Fixed

- ESLint no-case-declarations and no-useless-escape errors

## [1.1.0] - 2021-08-18

### Added

- Instructions for Chromium
- Button to reset all settings
- Automatic updating
- Issue tracker to package.json

### Changed

- Update instructions for Firefox
- Update dependencies

### Fixed

- Background in extension settings

### Removed

- Example section from documentation

## [1.0.0] - 2021-08-13

### Added

- Compatibility documentation
- Chromium browser support
- Browser style in settings
- Climate Strike license

### Changed

- Move libraries to subfolder
- Update CSS for options page
- Extend package.json

### Fixed

- Markdown rendering on Bitbucket

### Removed

- Source mapping URL from polyfill

## [0.4.1] - 2021-08-12

### Removed

- Loading of script when no file types are selected

## [0.4.0] - 2021-08-12

### Added

- Preferences documentation
- Notification setting
- Settings to page action
- .web-extension-id file

### Changed

- Use icon from Photon design system
- Use preferred color scheme in settings
- Allow clicking on setting labels
- Rename background and content scripts

## [0.3.1] - 2021-08-12

### Changed

- Enable JavaScript by default

### Removed

- .web-extension-id from .gitignore

## [0.3.0] - 2021-08-12

### Added

- Installation, usage and development instructions
- Setting to choose file types to monitor
- Node.js environment with web-ext and standard
- ESLint webextensions environment

### Changed

- Sort extension manifest keys

### Removed

- Linting of Live.js
- Developer manifest key

## [0.2.0] - 2021-08-10

### Added

- Extension icon
- Indicator page action
- German translation

### Changed

- Extend extension manifest

## [0.1.0] - 2021-08-09

### Added

- Initial release

[unreleased]: https://bitbucket.org/mpa_roesch/ddev-local-live.js-injector/branches/compare/HEAD..1.3.0
[1.3.0]: https://bitbucket.org/mpa_roesch/ddev-local-live.js-injector/commits/tag/1.3.0
[1.2.0]: https://bitbucket.org/mpa_roesch/ddev-local-live.js-injector/commits/tag/1.2.0
[1.1.0]: https://bitbucket.org/mpa_roesch/ddev-local-live.js-injector/commits/tag/1.1.0
[1.0.0]: https://bitbucket.org/mpa_roesch/ddev-local-live.js-injector/commits/tag/1.0.0
[0.4.1]: https://bitbucket.org/mpa_roesch/ddev-local-live.js-injector/commits/tag/0.4.1
[0.4.0]: https://bitbucket.org/mpa_roesch/ddev-local-live.js-injector/commits/tag/0.4.0
[0.3.1]: https://bitbucket.org/mpa_roesch/ddev-local-live.js-injector/commits/tag/0.3.1
[0.3.0]: https://bitbucket.org/mpa_roesch/ddev-local-live.js-injector/commits/tag/0.3.0
[0.2.0]: https://bitbucket.org/mpa_roesch/ddev-local-live.js-injector/commits/tag/0.2.0
[0.1.0]: https://bitbucket.org/mpa_roesch/ddev-local-live.js-injector/commits/tag/0.1.0
